﻿namespace ConsoleMenu
{
    public class Menu
    {
        protected Core _menu;
        protected List<Button> _buttons = new List<Button>();
        protected List<string> _text = new List<string>();
        protected bool _isLoad { get; private set; } = false;
        protected bool _isBlock = false;

        protected bool _exit = false;
        public virtual void Enter() => _menu.ExecuteButton();
        public virtual void Up() => _menu.UpSelect();
        public virtual void Down() => _menu.DownSelect();
        public virtual void Left() { }
        public virtual void Right() { }
        public virtual void Write()
        {
            if (!_isLoad)
            {
                _menu = new Core();
                _menu.LoadButton(_buttons);
                _isLoad = true;
            }
            _menu.LoadText(_text);
            Console.Clear();
            _menu.Buttons.ForEach(s =>
            {
                if (s.IsSelect) Console.BackgroundColor = s.ColorSelect;
                Console.WriteLine($"{s.Text}");
                Console.BackgroundColor = ConsoleColor.Black;
            });
            Console.WriteLine("====================================");
            _menu.Text.ForEach(s => Console.WriteLine(s));
        }

        public virtual bool Exit() => _exit;
        protected virtual void Log(string text) => _text.Add(text);

        public bool IsBlock() => _isBlock;
    }

}

