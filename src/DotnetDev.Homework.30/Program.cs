﻿using ConsoleMenu;
using DotnetDev.Homework._30;

public class Program
{
    private static void Main(string[] args) => new Visual(new Start()).Show();
}
