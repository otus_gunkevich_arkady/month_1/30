﻿namespace DotnetDev.Homework._30.Components
{
    /// <summary>
    /// Абстрактный класс любого объекта. 
    /// Определяет интерфейс для клонирования самого себя.
    /// </summary>
    public abstract class AnyObject
    {
        public int Id { get; set; }
        public AnyObject(int id)
        {
            Id = id;
        }
        public abstract string GetInfo();
    }
}
