﻿namespace DotnetDev.Homework._30.Components
{
    public class House : AnyObject, IClonable<House>
    {
        public int Number { get; set; }
        public House(int id, int number) : base(id)
        {
            Number = number;
        }
        public House Clone() => (House)MemberwiseClone();
        public House DeepClone() => new House(Id, Number);
        public override string GetInfo() => $"Дом № {Number} (Id = {Id})";

    }
}
