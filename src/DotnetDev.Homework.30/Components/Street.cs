﻿namespace DotnetDev.Homework._30.Components
{
    public class Street : AnyObject, IClonable<Street>
    {
        public string Name { get; private set; }
        public Street(int id, string name) : base (id)
        {
            Name = name;
        }
        public Street Clone() => (Street)MemberwiseClone();
        public Street DeepClone() => new Street(Id, Name);
        public override string GetInfo() => $"Улица {Name} (Id = {Id})";
    }
}
