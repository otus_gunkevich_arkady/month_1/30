﻿namespace DotnetDev.Homework._30.Components
{
    public interface IClonable<T>
    {
        T Clone();
        T DeepClone();
    }
}
