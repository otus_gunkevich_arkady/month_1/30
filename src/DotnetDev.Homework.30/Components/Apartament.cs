﻿namespace DotnetDev.Homework._30.Components
{
    /// <summary>
    /// Квартира. Для упрощения добавил только улицу и дом (без города и страны)
    /// </summary>
    public class Apartament : AnyObject, IClonable<Apartament>
    {
        /// <summary>
        /// Номер квартиры
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// Площадь (рандомная)
        /// </summary>
        public double Square { get; set; }
        /// <summary>
        /// Количество комнат (рандомное)
        /// </summary>
        public int Room { get; set; }
        /// <summary>
        /// Этаж
        /// </summary>
        public int Floor { get; set; }
        /// <summary>
        /// Дом
        /// </summary>
        public House House { get; set; }
        /// <summary>
        /// Улица
        /// </summary>
        public Street Street { get; set; }
        public Apartament(int id, int number, int floor, House house, Street street) : base(id)
        {
            Number = number;
            Random _rnd = new Random();
            Room = _rnd.Next(1, 4);
            Square = Room * 16 + _rnd.Next(5, 20) * Math.Abs(_rnd.NextDouble());
            Floor = floor;
            House = house;
            Street = street;
        }
        public Apartament Clone() => (Apartament)MemberwiseClone();
        public Apartament DeepClone() => new Apartament(Id + 1, Number + 1, Floor, House, Street);
        public override string GetInfo() => $"{Street.GetInfo()} \\ {House.GetInfo()} \\ Квартира №{Number} (Id = {Id}) Этаж {Floor} (ком. {Room}, площ. {Square.ToString("##0.00")})";
    }
}
