﻿using ConsoleMenu;
using DotnetDev.Homework._30.Components;

namespace DotnetDev.Homework._30
{
    public class Start : Menu
    {
        public Start()
        {
            UpdateButton();
            UpdateText();
        }
        private void UpdateButton()
        {
            _buttons = new List<Button>
            {
                new Button("Пример", () => Example()),
                new Button("[*] Выйти", () => _exit = true)
            };
        }
        private void UpdateText()
        {
            _text = new List<string>()
            {
                "",
                "",
                "Гункевич Аркадий Игоревич | OTUS",
                "ДЗ: Порождающие шаблоны проектирования"
            };
        }
        public void Example()
        {
            //Список улиц.
            var listStreet = new List<Street>
            {
                new Street(0, "Ленина"),
                new Street(1, "Пушкина")
            };
            //Список домов
            var listHouse = new List<House>();
            //Список квартир
            var listApartaments = new List<Apartament>();
            foreach (var street in listStreet)
            {
                //Например на каждой улице построено пока только по 2 дома
                for (int i = 0; i < 2; i++)
                {
                    var house = listHouse.FirstOrDefault(s => s.Id == i);
                    if (house is null)
                    {
                        house = new House(i, i + 1);
                    }
                    else
                    {
                        //Создаём новый дом, клонируя старый
                        house = house.Clone();
                        //Меняем ID, но оставляет номер дома
                        house.Id = listHouse.Count();
                    }
                    listHouse.Add(house);
                    //Например в каждом доме по 2 этажа и по 3 квартире на этаже
                    for (int j = 0; j < 2; j++)
                    {
                        //Создаём квартиру
                        var apartament = new Apartament(listApartaments.Count(), 1, j + 1, house, street);
                        //Добавляем первую квартиру
                        listApartaments.Add(apartament);
                        //Добавляем вторую квартиру
                        listApartaments.Add(apartament.DeepClone());
                        //Добавляем третью квартиру | Клонирование клона |
                        listApartaments.Add(apartament.DeepClone().DeepClone());
                    }
                }
            }
            _text = new List<string>();
            _text.AddRange(listApartaments.Select(s => s.GetInfo()));
        }
    }
}
